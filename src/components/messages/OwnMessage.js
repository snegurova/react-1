import React from "react";
import './Message.css'

class OwnMessage extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isEditing: false };
    this.handleDelete = this.handleDelete.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
    this.handleEditClick = this.handleEditClick.bind(this);
  }
  handleDelete() {
    this.props.onMessageDelete(this.props.messageId);
  }
  handleEdit(e) {
    this.setState({ isEditing: false });
    this.props.onMessageEdit(this.props.messageId, e.target.value);
  }
  handleEditClick() {
    this.setState({ isEditing: !this.state.isEditing });
  }
  render() {
    return (
      <li className="own-message">
        <div className="message-info">
          <span className="message-user-name">{this.props.userName} </span>
          <span className="message-time">at {this.props.messageTime}</span>
        </div>
        <div className="own-message-text">
          {this.props.messageText}
        </div>
        { this.state.isEditing
          ? <input type="text" onBlur={ this.handleEdit }/>
          : null
        }
        <div className="own-message-buttons">
          <button className="message-edit" onClick={ this.handleEditClick }>Edit</button>
          <button className="message-delete" onClick={ this.handleDelete }>Delete</button>
        </div>
      </li>
    );
  }
};

export default OwnMessage;