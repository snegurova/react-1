import React from "react";
import Header from "../Header/Header";
import './Chat.css'
import MessageList from "../MessageList/MessageList";
import Input from "../Input/Input";
import Preloader from "../Preloader/Preloader";
import { v4 as uuidv4 } from 'uuid';

class Chat extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      messages: [],
      isLoading: false
    };
    this.handleMessageDelete = this.handleMessageDelete.bind(this);
    this.handleMessageEdit = this.handleMessageEdit.bind(this);
    this.handleMessageAdd = this.handleMessageAdd.bind(this);
  }
  
  setChatData(messages) {
    const messagesCount = messages.length;
    const randomIndex = Math.floor(Math.random() * messagesCount);
    const usersCount = new Set(messages.map(it => it.userId)).size;
    const date = new Date( messages[messagesCount - 1]['createdAt']);
    const lastMessageDate = `${date.getDate()}.${date.getMonth() + 1}.${date.getFullYear()} ${date.getHours()}:${date.getMinutes()}`;
    this.setState({
      userId: messages[randomIndex].userId,
      chatTitle: messages[randomIndex].user,
      avatar: messages[randomIndex].avatar,
      user: messages[randomIndex].user,
      usersCount,
      messagesCount,
      lastMessageDate
    });
  }
  
  async componentDidMount() {
    this.setState({ isLoading: true });
    const response = await fetch(this.props.url);
    const messages = await response.json();
    const messagesSorted = messages.sort((a, b) => Date.parse(a['createdAt']) - Date.parse(b['createdAt']));
    this.setState({ messages: messagesSorted });
    await this.setChatData(messagesSorted);
    this.setState({ isLoading: false });
  }
  handleMessageDelete(id) {
    const messages = this.state.messages;
    for (let i = 0; i < messages.length; i++) {
      const message = messages[i];
      if (id === message.id) {
        messages.splice(i, 1);
        break;
      }
    }
    this.setState({
      messages,
      messagesCount: messages.length
    });
  }
  handleMessageEdit(id, value) {
    const messages = this.state.messages.map(it => {
      if (it.id === id) {
        it.text = value;
        it.editedAt = new Date(Date.now());
        return it;
      }
      return it;
    });
    this.setState({ messages });
  }
  handleMessageAdd(value) {
    const messages = this.state.messages;
    const date = new Date(Date.now());
    const message = {
      id: uuidv4(),
      userId: this.state.userId,
      avatar: this.state.avatar,
      user: this.state.user,
      text: value,
      createdAt: date,
      editedAt: ""
    }
    messages.push(message);
    const messagesCount = messages.length;
    const lastMessageDate = `${date.getDate()}.${date.getMonth() + 1}.${date.getFullYear()} ${date.getHours()}:${date.getMinutes()}`;
    this.setState({
      messages,
      messagesCount,
      lastMessageDate
    });
  }
  render() {
    if (this.state.isLoading) {
      return <Preloader />;
      
    }
    return (
      <div className="chat-wrapper">
        <Header
          title = {this.state.chatTitle}
          usersCount = {this.state.usersCount}
          messagesCount = {this.state.messagesCount}
          lastMessageDate = {this.state.lastMessageDate}
        />
        <MessageList
          messages = {this.state.messages}
          userId = {this.state.userId}
          onMessageDelete = { this.handleMessageDelete }
          onMessageEdit = { this.handleMessageEdit }
          
        />
        <Input onMessageAdd = { this.handleMessageAdd } />
      </div>
    );
  }
}

export default Chat;