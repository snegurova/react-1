import React from "react";
import './MessageList.css'
import Message from "../messages/Message";
import OwnMessage from "../messages/OwnMessage";

class MessageList extends React.Component {
  render() {
    const massagesListItems = this.props.messages.map(it => {
      const date = new Date(it.createdAt);
      if (it.userId === this.props.userId) {
        return <OwnMessage
          key = {it.id}
          messageId = {it.id}
          userName ={it.user}
          messageTime ={`${date.getHours()}:${date.getMinutes()}`}
          messageText ={it.text}
          onMessageDelete = { this.props.onMessageDelete }
          onMessageEdit = { this.props.onMessageEdit }
          onMessageAdd = { this.props.onMessageAdd }
        />;
      } else {
        return <Message
          key = {it.id}
          avatarUrl = {it.avatar}
          userName ={it.user}
          messageTime ={`${date.getHours()}:${date.getMinutes()}`}
          messageText ={it.text}
        />
      }
    });
    return (
      <ul className="message-list">
        {massagesListItems}
      </ul>
    );
  }
}

export default MessageList;