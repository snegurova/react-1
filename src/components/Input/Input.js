import React from "react";
import './Input.css'

class Input extends React.Component {
  constructor(props) {
    super(props);
    this.handleMessageAdd = this.handleMessageAdd.bind(this);
    this.onChange = this.onChange.bind(this);
  }
  onChange(e) {
    const value = e.target.value;
    this.setState({ messageText: value });
  }
  handleMessageAdd() {
    this.props.onMessageAdd(this.state.messageText);
  }
  render() {
    return (
      <div className="message-input">
        <input className="message-input-text" type="text" onChange={this.onChange}/>
        <button className="message-input-button" onClick={ this.handleMessageAdd }>Send</button>
      </div>
      
    );
  }
}

export default Input;