import React from "react";
import './Header.css'

class Header extends React.Component {
  render() {
    return (
      <header className="header">
        <div className="header-title">{this.props.title}</div>
        <div className="header-users-count">{this.props.usersCount} participants</div>
        <div>
          <span className="header-messages-count">{this.props.messagesCount}</span>
          <span> messages</span>
        </div>
        <div>
          <span>Last message at </span>
          <span className="header-last-message-date">{this.props.lastMessageDate}</span>
        </div>
        
      </header>
    );
  }
}

export default Header;