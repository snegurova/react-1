import './App.css';
import Chat from "./components/Chat/Chat";


function App() {
  const url = `https://edikdolynskyi.github.io/react_sources/messages.json`;
  return (
    <Chat url = {url} />
  );
};

export default App;
